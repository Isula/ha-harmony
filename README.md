# Haproxy-generator

This is used with s3 between a pair of haproxy load balancers to supply a docker swarm pool with self healing pools per virtual host.  Once a domain has been put in the sites.d directory ('touch www.example.com'), all it takes is just a re run of the template generator and it has it's own self healing view.  With this application containers are free to roam and scale the pool as the pool sees fit.

## Installation

This was created with Ubuntu 14.04, using the haproxy 1.6 PPA's.  Haproxy-generator only needs to be installed on the shared file system once.  Prerequisites need to be installed on each haproxy node.

### Install Ruby and Git

```code
sudo apt-get update && sudo apt-get install -y git ruby
```

### Install haproxy

```code
sudo apt-add-repository ppa:vbernat/haproxy-1.6 && sudo apt-get -y update && apt-get -y install haproxy
```

### Install s3fs-fuse

```code
sudo apt-add-repository ppa:apachelogger/s3fs-fuse && sudo apt-get -y update && apt-get -y install s3fs-fuse
echo '[aws_access_key]:[aws_secret_key]' > /etc/passwd-s3fs && chmod 0640 /etc/passwd-s3fs
ruby -e ' buf = File.open("/etc/rc.local","r").read.split /\n/
          buf[-1]="nohup s3fs [s3_bucket_name] /opt/haproxy > /dev/null &" 
          buf.push "exit 0"
          File.open("/etc/rc.local", "w") {|f| f.write(buf.join("\n")+"\n") }'
chmod 0755 /etc/rc.local
mkdir -p /opt/haproxy
s3fs [s3_bucket_name] /opt/haproxy
```

### Install haproxy-generator

```code
cd /opt/haproxy 
git clone git@gitlab.com:Isula/haproxy-generator.git .
```

## Usage

To add a virtual host to the haproxy envrionment, touch a file in sites.d with the fully qualified domain name as the file name.

To add a server to the pool, touch a file in the backends.d with the ip address of the server for the file name.

```code
stephanie@niribu /opt/haproxy $ ./generate_config.rb 
Sites:
["example.com", "www.example.com"]

Backends:
["127.0.0.1", "127.0.0.2", "127.0.0.3"]

Your new config is in /opt/haproxy/haproxy.cfg

```

Combine this with a symlink to /etc/haproxy/haproxy.cfg

```code
mv /etc/haproxy/haproxy.cfg /etc/haproxy/_haproxy.cfg
ln -s /opt/haproxy/haproxy.cfg /etc/haproxy/haproxy.cfg

service haproxy restart
```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b patch-my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin patch-my-new-feature`
5. Submit a pull request :D

